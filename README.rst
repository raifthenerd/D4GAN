D4GAN = Discrete *De novo* Drug Design using GAN
================================================

Authors
------------------------------------------------

- `Hyoshin Kim`_
- `Kyeongwon Lee`_
- `Seowon Choi`_
- `Seokjin Han`_

All authors are affiliated with `Departments of Statistics, Seoul National University`_.

.. _Departments of Statistics, Seoul National University: http://stat.snu.ac.kr/eng/
.. _Hyoshin Kim: https://github.com/figten
.. _Kyeongwon Lee: https://github.com/alpha195
.. _Seowon Choi: https://github.com/Thermobear
.. _Seokjin Han: http://blog.raifthenerd.com

Introduction
------------------------------------------------

Over the last decades, the discovery and development of novel drug candidates has been a challenging issue in the pharmaceutical industry. The cost of developing new drugs has been soaring nowadays. Recent work shows that the estimated cost of developing a single cancer drug was 648 million dollars, and the launching price for a new drug has doubled over the past two decades. Failures in the development process are key contributors to this cost. Estimated clinical approval success rate is only 11.8% [1]_.

The very beginning step of new drug developing process, the prehuman/preclinical step, where researchers search for potential drug candidates from large molecule database, takes its share about 28.6% in the total cost of a drug development trial [2]_. This pilot study is definitely one of the biggest driver of the cost in pharmaceutical industrial innovation aside for Phase III stage. Recently, massively stacking datasets on molecular structures and their biochemical properties make this process more efficient and comprehensive. But at the same time, demand for more effective methods that guarantee both diversity and druglikeliness of potential molecular candidates is a rising concern [3]_.

New generative methods in deep learning have gained interest in Computer Assisted de novo Drug Design (CADD). In mainstream, methods using Qualitative Structure Activity Relationship (QSAR) or Molecular Docking have been suggested in the new drug design [4]_ [5]_. These methods associate particular molecular structures or ligands to the chemical properties of the molecule with several data analysis techniques, such as linear regression, SVM, stochastic tree search etc. But because of too much complexity and variablity of drug-like molecule family, the methods using statistics, combinatorics or machine learning have had limited ability to fully analyse and generate potential candidates.

Recently, with the advent of Deep Neural Network techniques (DNN) and parallel computing, researchers have enjoyed more complicated non-linear models on the design of novel molecules. Models using neural networks like long short-term memory (LSTM) [6]_, variational autoencoder (VAE) [7]_ or RL-based generative adversarial networks (GAN) [8]_ have been suggested, and a reinforcement learning based language model have also been adopted in generating sample molecules [9]_. These model think of molecules as a sequence of atoms, so used SMILES (simplified molecular-input line-entry system) for describing the grammar of how the atoms are composed to build the structure of the given molecule. Another state-of-the-art model, which think of molecules as a sequence of chemical reactions, adopted Monte Carlo Tree Search (MCTS) also showed compatible potential for generating new molecules [10]_.

We propose a new drug design model called D4GAN. We used boundary-seeking GAN (BSGAN) [11]_, and Objective-Reinforced Generative Adversarial Networks (ORGAN) [8]_, an approach for training a generative model adversarially with discrete data, which is expected to directly deal with the discrete sequential nature of SMILES dataset in a stable manner, respectively. We adopt Wasserstein GAN (WGAN) which uses Wasserstein distance for assessing loss of the generated sample, to compare the convergence and quality of generated de novo drug candidates with BSGAN model [12]_. We also tried VAE for the generator in WGAN, to improve the stability and quality of sample model generation, avoiding the mode collapse that often happens in other GAN models [13]_.

We test our model in the context of drug generation, optimizing several molecule metrics. Our results show that D4GAN successfully tunes the structure and quality of generated samples.

References
------------------------------------------------

.. [1] Prasad, V. and Mailankody, S. (2017). Research and development spending to bring a single cancer drug to market and revenues after approval. *JAMA internal medicine*, 177(11):1569–1575.
.. [2] Avorn, J. (2015). The $2.6 billion pill-methodologic and policy considerations. *New England Journal of Medicine*, 372(20):1877–1879.
.. [3] Roy, A. S. (2012). Stifling new cures: the true cost of lengthy clinical drug trials. *Manhattan Institute for Policy Research*, 5:5–13.
.. [4] Meng, X.-Y., Zhang, H.-X., Mezei, M., and Cui, M. (2011). Molecular docking: a powerful approach for structure-based drug discovery. *Current computer-aided drug design*, 7(2):146–157.
.. [5] Cherkasov, A., Muratov, E. N., Fourches, D., Varnek, A., Baskin, I. I., Cronin, M., Dearden, J., Gramatica, P., Martin, Y. C., Todeschini, R., et al. (2014). Qsar modeling: where have you been? where are you going to? *Journal of medicinal chemistry*, 57(12):4977–5010.
.. [6] Segler, M. H., Kogej, T., Tyrchan, C., and Waller, M. P. (2017). Generating focused molecule libraries for drug discovery with recurrent neural networks. *ACS Central Science*.
.. [7] Blaschke, T., Olivecrona, M., Engkvist, O., Bajorath, J., and Chen, H. (2017). Application of generative autoencoder in de novo molecular design. *Molecular informatics*.
.. [8] Guimaraes, G. L., Sanchez-Lengeling, B., Outeiral, C., Farias, P. L. C., and Aspuru-Guzik, A. (2017). Objective-reinforced generative adversarial networks (organ) for sequence generation models. *arXiv preprint arXiv:1705.10843*.
.. [9] Olivecrona, M., Blaschke, T., Engkvist, O., and Chen, H. (2017). Molecular de-novo design through deep reinforcement learning. *Journal of cheminformatics*, 9(1):48.
.. [10] Segler, M. H., Preuss, M., and Waller, M. P. (2018). Planning chemical syntheses with deep neural networks and symbolic ai. *Nature*, 555(7698):604.
.. [11] Hjelm, R. D., Jacob, A. P., Che, T., Trischler, A., Cho, K., and Bengio, Y. (2018). Boundary-seeking generative adversarial networks. *arXiv preprint arXiv:1702.08431v4*.
.. [12] Arjovsky, M., Chintala, S., and Bottou, L. (2017). Wasserstein GAN. *arXiv preprint arXiv:1701.07875v3*.
.. [13] Larsen, A. B. L., Sønderby, S. K., Larochelle, H., and Winther, O. (2015). Autoencoding beyond pixels using a learned similarity metric. *arXiv preprint arXiv:1512.09300v2*.

**Disclaimer.** The implementation of D4GAN is based on the code from `gablg1/ORGAN`_.

.. _gablg1/ORGAN: https://github.com/gablg1/ORGAN

Requirements
------------------------------------------------

See `requirements.yml`_.

.. _requirements.yml: requirements.yml
