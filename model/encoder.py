"""Encoder network."""

import torch
import torch.nn as nn
from utils.smiles import VOCAB


class Enc(nn.Module):
  """Encoder network; maps given SMILES sequences into the feature space.

  The encoder is consist of following layers:

  - Embedding layer.
  - GRU layer.
  - Two fully-connected layers; correspond to the :math:`\\mu` and
    :math:`\\log\\sigma`, respectfully.

  Args:
    seq_len (int): Maximum length.
    feature_dim (int): Dimension of feature space.
    embed_dim (int): Dimension of embedding tensors.
    device (:obj:`torch.Device`): PyTorch device.
  """
  def __init__(self, seq_len, feature_dim, embed_dim, device):
    super(Enc, self).__init__()
    # Constants
    self.seq_len = seq_len
    self.feature_dim = feature_dim
    self.embed_dim = embed_dim
    self.device = device
    # Layers
    self.embed = nn.Embedding(len(VOCAB)+2, self.embed_dim)
    self.gru = nn.GRU(self.embed_dim, self.feature_dim,
                      batch_first=True)
    self.mu = nn.Linear(self.feature_dim, self.feature_dim)
    self.log_sigma =  nn.Linear(self.feature_dim, self.feature_dim)

  def forward(self, x):
    """
    Args:
      x (:obj:`torch.LongTensor`): SMILES sequences. The dimension is
        ``[batch_size, seq_len]``.

    Returns:
      (:obj:`torch.Tensor`, :obj:`torch.Tensor`): :math:`\\mu` and
      :math:`\\log\\sigma`.

      The dimension of :math:`\\mu` is ``[batch_size, feature_dim]``.

      The dimension of :math:`\\log\\sigma` is ``[batch_size, feature_dim]``.
    """
    batch_size, _ = x.size()
    o = self.embed(x)  # o: [batch_size, seq_len, hidden_dim]
    h = torch.zeros(1, batch_size, self.feature_dim,
                    device=self.device)
    _, h = self.gru(o, h)  # h: [1, batch_size, hidden_dim]
    h = h.squeeze(0)
    return self.mu(h), self.log_sigma(h)
