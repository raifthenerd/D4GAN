"""Model specifications of the networks used in D4GAN."""

import torch
from utils.smiles import VOCAB
from .encoder import Enc
from .generator import Gen
from .discriminator import Dis

class D4GAN:
  """D4GAN = Discrete *De novo* Drug Design using GAN.

  D4GAN is consist of following networks (inspired by [#]_):

  - Encoder network; maps given SMILES sequences into the feature space.
  - Generator network; generates SMILES sequences from given feature.
  - Discriminator network; classifiy given SMILES sequences are genuine or not.

  The encoder and discriminator can be considered as auxiliary networks for
  effective training of generator. For more infos about training the model, see
  docstrings in ``loss`` module.

  Args:
    seq_len (int): Maximum length.
    feature_dim (int): Dimension of feature space.
    e_embed_dim (int): Dimension of encoder's embedding tensors.
    g_embed_dim (int): Dimension of generator's embedding tensors.
    d_embed_dim (int): Dimension of discriminator's hidden tensors.

  .. [#] Larsen, A. B. L., Sønderby, S. K., Larochelle, H., & Winther, O.
    (2015). Autoencoding beyond pixels using a learned similarity metric.
    *arXiv preprint arXiv:1512.09300*.
  """
  def __init__(self, seq_len, feature_dim,
               e_embed_dim, g_embed_dim, d_hidden_dim):
    # Constants
    self.seq_len = seq_len
    self.feature_dim = feature_dim
    self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # Networks
    self.enc = Enc(self.seq_len, self.feature_dim, e_embed_dim, self.device)
    self.gen = Gen(self.seq_len, self.feature_dim, g_embed_dim, self.device)
    self.dis = Dis(self.seq_len, d_hidden_dim, self.device)
    self.enc.to(self.device)
    self.gen.to(self.device)
    self.dis.to(self.device)

  def sample(self, features, num_samples=1, teacher=None):
    """Sampling SMILES sequences from generator network.

    Args:
      features (:obj:`torch.Tensor`): Features. The dimension is
        ``[batch_size, feature_dim]``
      num_samples (int, optional): Number of samples generated from each single
        feature.
      teacher (:obj:`torch.Tensor`, optional): Ground-truth SMILES sequences
        for teacher forcing training. The dimension is
        ``[num_samples x batch_size, seq_len]``.

    Returns:
      (:obj:`torch.Tensor`, :obj:`torch.Tensor`): Generated samples and
      corresponding loglikelihood tensor.

      The dimension of sample tensor is
      ``[batch_size * num_samples, seq_len]``.

      The dimension of loglikelihood tensor is
      ``[batch_size * num_samples, vocab_size, seq_len]``.
    """
    num_features = features.size()[0]
    fakes = torch.zeros(num_samples * num_features, self.seq_len,
                        dtype=torch.long, device=self.device)
    log_g = torch.zeros(num_samples * num_features, len(VOCAB)+2, self.seq_len,
                        device=self.device)
    c = torch.zeros(num_samples * num_features,
                    dtype=torch.long, device=self.device)
    eos = torch.ones(num_samples * num_features,
                     dtype=torch.long, device=self.device)

    z = features.unsqueeze(0)   # z: [1, num_features, feature_dim]
    z = z.expand(num_samples, -1, -1).contiguous() # z: [num_samples, num_features, feature_dim]
    z = z.view(1, -1, self.feature_dim) # z: [1, num_samples x num_features, feature_dim]
    for i in range(self.seq_len):
      o, z = self.gen.forward(c, z) # o: [num_samples x num_features, vocab_size]
                                    # z: [1, num_samples x num_features, feature_dim]
      c = torch.multinomial(torch.exp(o), 1).view(-1) # c: [num_samples x num_features]
      if teacher is None:
        eos *= (c != 1).to(dtype=torch.long, device=self.device) # eos: [num_samples x num_features]
        c = eos * c + (1-eos)   # c: [num_samples x num_features]
      fakes[:, i] = c
      log_g[:, :, i] = o
      if teacher is not None:
        c = teacher[:, i]       # c: [num_samples x num_features]
    return fakes, log_g
