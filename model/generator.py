"""Generator network."""

import torch.nn as nn
import torch.nn.functional as F
from utils.smiles import VOCAB


class Gen(nn.Module):
  """Generator network; generates SMILES sequences from given feature.

  The generator is consist of following layers:

  - Embedding layer.
  - GRU layer.
  - Fully-connected layer with log-softmax activation.

  Args:
    seq_len (int): Maximum length.
    feature_dim (int): Dimension of feature space.
    embed_dim (int): Dimension of embedding tensors.
    device (:obj:`torch.device`): PyTorch Device.
  """
  def __init__(self, seq_len, feature_dim, embed_dim, device):
    super(Gen, self).__init__()
    # Constants
    self.seq_len = seq_len
    self.feature_dim = feature_dim
    self.embed_dim = embed_dim
    self.device = device
    # Layers
    self.embed = nn.Embedding(len(VOCAB)+2, self.embed_dim)
    self.gru = nn.GRU(self.embed_dim, self.feature_dim)
    self.fc = nn.Linear(self.feature_dim, len(VOCAB)+2)

  def forward(self, x, z):
    """
    Args:
      x (:obj:`torch.LongTensor`): Current characters. The dimension is
        ``[batch_size]``.
      z (:obj:`torch.LongTensor`): Hidden states. The dimension is
        ``[1, batch_size, feature_dim]``.

    Returns:
      (:obj:`torch.Tensor`, :obj:`torch.Tensor`): Output and hidden tensor.

      The dimension of output tensor is ``[batch_size, vocab_size]``.

      The dimension of hidden tensor is ``[1, batch_size, hidden_dim]``.
    """
    o = F.relu(self.embed(x))   # o: [batch_size, hidden_dim]
    o = o.unsqueeze(0)          # o: [1, batch_size, hidden_dim]
    o, z = self.gru(o, z)       # o: [1, batch_size, feature_dim]
                                # z: [1, batch_size, feature_dim]
    o = o.squeeze(0)            # o: [batch_size, feature_dim]
    o = self.fc(o)              # o: [batch_size, vocab_size]
    o = F.log_softmax(o, dim=1) # o: [batch_size, vocab_size]
    return o, z
