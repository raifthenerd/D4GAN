"""Discriminator network."""

import torch
import torch.nn as nn
import torch.nn.functional as F
from utils.smiles import VOCAB


class Dis(nn.Module):
  """Discriminator network; classifiy given SMILES sequences are genuine or
  not.

  The discriminator is consist of following layers:

  - 1D convolution layer with batch normalization.
  - 1D convolution layer with leaky ReLU and batch normalization.
  - Fully-connected layer.

  Args:
    seq_len (int): Maximum length.
    hidden_dim (int): Dimension of hidden tensors.
    device (:obj:`torch.device`): PyTorch Device.
  """
  def __init__(self, seq_len, hidden_dim, device):
    super(Dis, self).__init__()
    # Constants
    self.seq_len = seq_len
    self.hidden_dim = hidden_dim
    self.device = device
    # Layers
    self.conv = nn.Sequential(
      nn.Conv1d(len(VOCAB)+2, self.hidden_dim,
                kernel_size=5, stride=1, padding=2, bias=False),
      nn.BatchNorm1d(self.hidden_dim)
    )
    self.cnn = nn.Sequential(
      nn.LeakyReLU(0.2),
      nn.Conv1d(self.hidden_dim, self.hidden_dim,
                kernel_size=5, stride=1, padding=2, bias=False),
      nn.BatchNorm1d(self.hidden_dim)
    )
    self.fc = nn.Linear(self.hidden_dim * self.seq_len, 1)

  def forward(self, x):
    """
    Args:
      x (:obj:`torch.tensor`): SMILES sequences. The dimension is
        ``[batch_size, seq_len]``.

    Returns:
      :obj:`torch.tensor`: Output tensor.

      The dimension is ``[batch_size]``.
    """
    o = torch.zeros(x.size()[0], len(VOCAB)+2, self.seq_len,
                    device=self.device)
    o.scatter_(1, x.view(-1, 1, self.seq_len), 1)
    o = self.conv(o)            # o: [batch_size, hidden_dim, seq_len]
    o = self.cnn(o) + o         # o: [batch_size, hidden_dim, seq_len]
    o = o.view(-1, self.hidden_dim * self.seq_len) # o: [batch_size, hidden_dim x seq_len]
    o = F.leaky_relu(o, 0.2)    # o: [batch_size, hidden_dim x seq_len]
    o = self.fc(o)              # o: [batch_size, 1]
    return o.view(-1)
