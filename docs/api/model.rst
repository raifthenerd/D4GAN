``model``
=====================================================
.. automodule:: model
   :members:
      D4GAN,
      Enc,
      Gen,
      Dis
