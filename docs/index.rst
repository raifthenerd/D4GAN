.. include:: ../README.rst

API
-----------------------------------------------------
.. toctree::
   :maxdepth: 2

   api/model
   api/loss
   api/utils
