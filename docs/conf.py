# -*- coding: utf-8 -*-

import os
import sys
sys.path.insert(0, os.path.abspath('..'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.todo',
    'sphinx.ext.napoleon',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
]
autodoc_mock_imports = ['torch', 'rdkit', 'numpy']
autodoc_member_order = 'bysource'
napoleon_include_init_with_doc = True

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'

project = 'D4GAN'
copyright = '2018, Team D4GAN'
author = 'Team D4GAN'

version = ''
release = ''
language = 'en'

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'
todo_include_todos = True

html_theme = 'alabaster'
# html_theme_options = {}
html_static_path = ['_static']
html_show_sourcelink = False
html_sidebars = {
  "**": ['localtoc.html',
         'relations.html',
         'searchbox.html']
}
