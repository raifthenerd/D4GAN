"""D4GAN = Discrete De novo Drug Design using GAN"""

import os
import logging
import argparse
import traceback
from datetime import datetime
from itertools import cycle

import torch
import numpy as np
from torch.optim import Adam
from torch.utils.data import DataLoader
from rdkit import rdBase

import model
import loss
import utils
import utils.chem
import utils.smiles


logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(filename)s:%(lineno)d %(message)s")
log = logging.getLogger("D4GAN")
rdBase.DisableLog("rdApp.error")


def train():
  """Load data and train the model."""
  parser = argparse.ArgumentParser()
  parser.add_argument("--config", type=str, default="config.yml",
                      help="Configuration file.")
  parser.add_argument("--data", type=str, default="data/drugs_sub.smi",
                      help="Configuration file.")
  args = parser.parse_args()

  t_start = datetime.now()
  jobid = t_start.strftime('%y%m%d-%H%M%S')
  os.makedirs("log/%s" % jobid, exist_ok=True)
  log.addHandler(logging.FileHandler("log/%s.log" % jobid))
  log.info("[Start job] %s", t_start)

  config = utils.load_config(args.config)
  log.info("[Config loaded]")
  for k, v in config.items():
    log.info("  %s: %s", k, v)

  smiles = utils.smiles.load_smi(args.data)
  metric_fns = utils.chem.load_metrics(config["CHEM_METRICS"], smiles)
  log.info("[Data loaded]")
  log.info("  Number of data: %d", len(smiles))
  for metric in config["CHEM_METRICS"]:
    values = metric_fns[metric](smiles)
    log.info("  Summary statistics for %s:", metric)
    log.info("    mean: %f / sd: %f", np.mean(values), np.std(values))
    log.info("    min: %f / Q1: %f / Q2: %f / Q3: %f / max: %f",
             np.min(values),
             *np.percentile(values, [25, 50, 75]),
             np.max(values))
  lens = list(map(len, smiles))
  seq_len = int(np.percentile(lens, 99, interpolation='nearest'))
  log.info("  Summary statistics for length of data:")
  log.info("    mean: %f / sd: %f", np.mean(lens), np.std(lens))
  log.info("    min: %d / Q1: %d / Q2: %d / Q3: %d / max: %d",
           np.min(lens),
           *np.percentile(lens, [25, 50, 75], interpolation='nearest'),
           np.max(lens))

  encodeds = torch.LongTensor([utils.smiles.encode(s, seq_len)
                               for s in smiles
                               if utils.smiles.verify_with_maxlen(s, seq_len)])
  batches = DataLoader(dataset=encodeds, batch_size=config['BATCH_SIZE'],
                       shuffle=True, num_workers=1)

  gan = model.D4GAN(seq_len,
                    config['Z_DIM'],
                    config['ENC_EMBED_DIM'],
                    config['GEN_EMBED_DIM'],
                    config['DIS_HIDDEN_DIM'])
  opt_enc = Adam(gan.enc.parameters(), lr=config['ENC_LEARNING_RATE'])
  opt_gen = Adam(gan.gen.parameters(), lr=config['GEN_LEARNING_RATE'])
  opt_dis = Adam(gan.dis.parameters(), lr=config['DIS_LEARNING_RATE'])

  metric_iter = cycle(metric_fns.values())
  for epoch in range(config['NUM_EPOCHS_TRAIN']):
    log.info("[Training Epoch %d]", epoch+1)
    for batch in batches:
      loss_enc, loss_gen, loss_dis = loss.train(gan, batch.to(gan.device),
                                                config['NUM_SAMPLES'],
                                                config['LAMBDA'],
                                                config['KAPPA'],
                                                config['TAU'],
                                                next(metric_iter))
      log.info("Loss: E %f, G %f, D %f",
               loss_enc.item(), loss_gen.item(), loss_dis.item())
      opt_enc.zero_grad()
      loss_enc.backward(retain_graph=True)
      opt_enc.step()
      opt_gen.zero_grad()
      loss_gen.backward(retain_graph=True)
      opt_gen.step()
      opt_dis.zero_grad()
      loss_dis.backward()
      opt_dis.step()
    fakes, _ = gan.sample(torch.randn(100, gan.feature_dim, device=gan.device))
    fakes = [utils.smiles.decode(fake) for fake in fakes]
    utils.smiles.save_smi("log/%s/epoch_%d.smi" % (jobid, epoch+1), fakes)
    fakes = utils.smiles.drop_invalid(fakes)
    log.info("  %d/%d molecules are valid.", len(fakes), 100)
    if len(fakes) > 0:
      for metric in config["CHEM_METRICS"]:
        values = metric_fns[metric](fakes)
        log.info("  Summary statistics for %s:", metric)
        log.info("    mean: %f / sd: %f", np.mean(values), np.std(values))
        log.info("    min: %f / Q1: %f / Q2: %f / Q3: %f / max: %f",
                 np.min(values),
                 *np.percentile(values, [25, 50, 75]),
                 np.max(values))
  fakes, _ = gan.sample(torch.randn(5000, gan.feature_dim, device=gan.device))
  fakes = [utils.smiles.decode(fake) for fake in fakes]
  utils.smiles.save_smi("log/%s/result.smi" % jobid, fakes)
  fakes = utils.smiles.drop_invalid(fakes)
  log.info("  %d/%d molecules are valid.", len(fakes), 5000)
  if len(fakes) > 0:
    for metric in config["CHEM_METRICS"]:
      values = metric_fns[metric](fakes)
      log.info("  Summary statistics for %s:", metric)
      log.info("    mean: %f / sd: %f", np.mean(values), np.std(values))
      log.info("    min: %f / Q1: %f / Q2: %f / Q3: %f / max: %f",
               np.min(values),
               *np.percentile(values, [25, 50, 75]),
               np.max(values))
  t_end = datetime.now()
  log.info("[Finish job] %s", t_end)
  log.info("  Time: %s", t_end - t_start)


if __name__ == "__main__":
  try:
    train()
    utils.send_email("D4GAN: JOB FINISHED", "See lk1.snu.ac.kr.")
  except:
    log.error(traceback.format_exc())
    utils.send_email("D4GAN: JOB FAILED", "See lk1.snu.ac.kr.")
