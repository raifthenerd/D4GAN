"""Helper functions for handling SMILES data.

SMILES (simplified molecular-input line-entry system) is a specification in
the form of a line notation for describing the structure of chemical species
using short ASCII strings.
"""

from rdkit.Chem import MolFromSmiles, MolToSmiles

# Construct SMILES character set
# Atoms; Replace Cl <-> Q and Br <-> W
VOCAB = ['H', 'B', 'c', 'C', 'n', 'N', 'o', 'O', 'p', 'P', 's', 'S', 'F', 'Q',
         'W', 'I']
# Atom modifiers
# - brackets [, ]
# - positive charge + (+), u (+2), y (+3)
# - negative charge ~ (-), ! (-2), & (-3)
# - added explicit hidrogens as Z (H2) and X (H3)
VOCAB = VOCAB + ['[', ']', '+', 'u', 'y', '~', '!', '&', 'Z', 'X']
# Bonds
VOCAB = VOCAB + ['-', '=', '#']
# Directional bonds
VOCAB = VOCAB + ['/', '\\']
# Branches
VOCAB = VOCAB + ['(', ')']
# Cycles
VOCAB = VOCAB + ['1', '2', '3', '4', '5', '6', '7']
# Counter-clockwise
VOCAB = VOCAB + ['@']
"char list: SMILES character set."

START_CHAR = '^'
"char: Character for denoting the start of a SMILES sequence."
PAD_CHAR = '_'
"char: Character used in padding."

# Dictionaries for encode/decode.
_CHAR_DICT = {START_CHAR: 0, PAD_CHAR: 1}
for _i, _c in enumerate(VOCAB):
  _CHAR_DICT[_c] = _i + 2
_ORD_DICT = {v: k for k, v in _CHAR_DICT.items()}


def load_smi(filename):
  """Load SMILES file.

  Args:
    filename (str): Path to SMILES.

  Returns:
    str list: SMILES sequences.
  """
  with open(filename) as f:
    return [smile.strip() for smile in f.readlines()]

def save_smi(filename, seqs):
  """Save SMILES sequences to a file.

  Args:
    filename (str): Path to SMILES.
    seqs (str list): SMILES to be saved.
  """
  with open(filename, 'w') as f:
    f.write('\n'.join(seqs) + '\n')

def pad(seq, n):
  """Add the padding character to a SMILES sequence until it is of length
  :math:`n`.

  Args:
    seq (str): A SMILES sequence.
    n (int): Maximum sequence length.

  Returns:
    str: A padded sequence of length :math:`n`.
  """
  if n < len(seq):
    return seq
  return seq + PAD_CHAR * (n - len(seq))

def unpad(seq):
  """Remove the padding characters.

  Args:
    seq (str): A SMILES sequence.

  Returns:
    str: Unpadded sequence.
  """
  return seq.rstrip(PAD_CHAR)

def encode(seq, n):
  """Encode a SMILES sequence into a integer sequence.

  Args:
    seq (str): A SMILES sequence.
    n (int): Maximum sequence length.

  Returns:
    int list: An encoded sequence.
  """
  # replace double char atoms symbols
  seq = seq.replace('Cl', 'Q').replace('Br', 'W')
  atom_spec = False
  new_chars = [''] * n
  i = 0
  for c in seq:
    if c == '[':
      atom_spec = True
      spec = []
    if atom_spec:
      spec.append(c)
    else:
      new_chars[i] = c
      i = i + 1
    # close atom spec
    if c == ']':
      atom_spec = False
      spec = ''.join(spec)
      # negative charges
      spec = spec.replace('-3', '&').replace('-2', '!').replace('-', '~')
      # positive charges
      spec = spec.replace('+3', 'y').replace('+2', 'u')
      # hydrogens
      spec = spec.replace('H2', 'Z').replace('H3', 'X')
      new_chars[i:i + len(spec)] = spec
      i = i + len(spec)
  return [_CHAR_DICT[c] for c in pad(''.join(new_chars), n)]

def decode(seq):
  """Decode a integer sequence into a SMILES sequence.

  Args:
    seq (:obj:`torch.LongTensor`): An encoded sequence.

  Returns:
    str: A SMILES sequence.
  """
  seq = unpad(''.join([_ORD_DICT[o.item()] for o in seq]))
  # negative charges
  seq = seq.replace('~', '-').replace('!', '-2').replace('&', '-3')
  # positive charges
  seq = seq.replace('y', '+3').replace('u', '+2')
  # hydrogens
  seq = seq.replace('Z', 'H2').replace('X', 'H3')
  # replace proxy atoms for double char atoms symbols
  seq = seq.replace('Q', 'Cl').replace('W', 'Br')
  return seq

def canonical_form(seq):
  """Transform a SMILES sequence to canonical form.

  Args:
    seq (str): A SMILES sequence.

  Returns:
    str: A canonical SMILES sequence.
  """
  return MolToSmiles(MolFromSmiles(seq))

def verify(seq):
  """Verify a given SMILES sequence is valid.

  Args:
    seq (str): A SMILES sequence.

  Returns:
    bool: ``True`` for a valid SMILES sequence. ``False`` otherwise.
  """
  if seq != '':
    mol = MolFromSmiles(seq)
    return mol is not None and mol.GetNumAtoms() > 1
  return False

def verify_with_maxlen(seq, n):
  """Verify if the SMILES sequence is valid and its length is less than
  :math:`n`.

  Args:
    seq (str): A SMILES sequence.
    n (int): Maximum sequence length.

  Returns:
    bool: ``True`` for a valid and short sequence. ``False`` otherwise.
  """
  return len(seq) <= n and verify(seq)

def verify_and_apply(seq, fn, **kwargs):
  """Verify a given SMILES sequence, and convert the sequence to a molecule and
  apply a function.

  Args:
    seq (str): A SMILES sequence.
    fn: A score function with an :obj:`rdkit.Mol` type argument.
    **kwargs: Arbitrary keyword arguments.

  Returns:
    float: If the sequence is valid, returns ``fn(mol, **kwargs)`` where
    ``mol`` is the converted SMILES sequence. Returns 0.0 otherwise.
  """
  if seq != '':
    mol = MolFromSmiles(seq)
    if mol is not None and mol.GetNumAtoms() > 1:
      try:
        return fn(mol, **kwargs)
      except Exception:
        return 0.0
  return 0.0

def drop_invalid(seqs):
  """Drop invalid SMILES sequences.

  Args:
    seqs (str list): List of SMILES sequences.

  Returns:
    str list: List of valid SMILES sequences.
  """
  return [seq for seq in seqs if verify(seq)]
