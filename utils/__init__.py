"""Various auxiliary functions.

Note:
  All implementations of this module and all submodules are based on the code
  of ORGAN_. We add some additional modifications. Also, we disclaim that
  some of the docstrings are migrated from another project called ORGANIC_.

.. _ORGAN: https://github.com/gablg1/ORGAN
.. _ORGANIC: https://github.com/aspuru-guzik-group/ORGANIC
"""

import os
import logging
import yaml
import requests
import numpy as np
import torch

# Default configurations
_CONFIG = {
    'BATCH_SIZE': 64,
    'NUM_EPOCHS_TRAIN': 100,
    'CHEM_METRICS': ['solubility', 'synthesizability', 'druglikeness'],
    'Z_DIM': 512,
    'NUM_SAMPLES': 16,
    'LAMBDA': 0.5,
    'KAPPA': 4,
    'TAU': 0.9,
    'ENC_EMBED_DIM': 32,
    'ENC_LEARNING_RATE': 0.001,
    'GEN_EMBED_DIM': 32,
    'GEN_LEARNING_RATE': 0.001,
    'DIS_HIDDEN_DIM': 128,
    'DIS_LEARNING_RATE': 0.00001,
}

def load_config(filename):
  """Load configuration file.

  Note:
    The configuration file should be in YAML_ format.

  Args:
    filename (str): Path of the configuration file.

  Returns:
    dict: Dictionary holds various configurations.

  .. _YAML: http://yaml.org/
  """
  try:
    with open(filename) as f:
      _CONFIG.update(yaml.load(f.read()))
  except FileNotFoundError:
    log = logging.getLogger(__name__)
    log.warning("Config file not found; use default configuration.")
  return _CONFIG

def send_email(subject, text):
  """Send email via Mailgun_.

  Note:
    You should set the API key as the environment variable ``MAILGUN_KEY``.

  Args:
    subject (str): Mail subject.
    text (str): Mail text.

  .. _Mailgun: https://mailgun.com
  """
  requests.post("https://api.mailgun.net/v3/raifthenerd.com/messages",
                auth=("api", os.environ.get("MAILGUN_KEY")),
                data={"from": "D4GAN <d4gan@raifthenerd.com>",
                      "to": ["raifthenerd@snu.ac.kr"],
                      "subject": subject,
                      "text": text})

def logsumexp(x, axis=None):
  """Numerically stable version of :math:`\\log(\\sum e^x)`.

  Args:
    x (:obj:`torch.Tensor`): :math:`x`.
    axis (int): Axis which a sum is performed.

  Returns:
    :obj:`torch.Tensor`: :math:`\\log(\\sum e^{x})`
  """
  x_max = torch.max(x, axis)[0]
  return torch.log((torch.exp(x - x_max)).sum(axis)) + x_max

def remap(x, xmin, xmax):
  """Remaps a given value to :math:`[0,1]` with a linear scale.

  Args:
    x (float): Value to be remapped.
    xmin (float): Minimum value.
    xmax (float): Maximum value.

  Returns:
    float: Linearly scaled value.
    If :math:`x<x_{\\min}`, returns 0.
    If :math:`x>x_{\\max}`, returns 1.
  """
  return np.clip((x - xmin) / (xmax - xmin), 0., 1.)
