"""Solubility."""

from rdkit.Chem import Crippen
from utils import remap
from utils.smiles import verify_and_apply

LOW_LOGP = -2.12178879609
HIGH_LOGP = 6.0429063424

def _evaluate(mol):
  return remap(Crippen.MolLogP(mol), LOW_LOGP, HIGH_LOGP)

def evaluate(seqs):
  """This metric computes the logarithm of the water-octanol partition
  coefficient, using RDKit_'s implementation of Wildman-Crippen method, and
  then remaps it to the :math:`[0,1]` range. See [#]_ for more information.

  Args:
    seqs (str list): SMILES sequences.

  Returns:
    float list: Scores.

  .. [#] Wildman, S. A., & Crippen, G. M. (1999). Prediction of physicochemical
    parameters by atomic contributions. Journal of chemical information and
    computer sciences, 39(5), 868-873.
  .. _RDKit: http://www.rdkit.org/
  """
  return [verify_and_apply(seq, _evaluate)
          for seq in seqs]
