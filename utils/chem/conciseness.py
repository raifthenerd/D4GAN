"""Conciseness."""

import numpy as np
from utils.smiles import canonical_form, verify

def _evaluate(seq):
  canon = canonical_form(seq)
  diff_len = len(seq) - len(canon)
  val = np.clip(diff_len, 0.0, 20)
  val = 1 - 1.0 / 20.0 * val
  return val

def evaluate(seqs):
  """This metric penalizes SMILES strings that are too long, assuming that the
  canonical representation is the shortest.

  Args:
    seqs (str list): SMILES sequences.

  Returns:
    float list: Scores.
  """
  return [_evaluate(seq) if verify(seq) else 0
          for seq in seqs]
