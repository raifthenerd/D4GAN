"""Novelty."""

from utils.smiles import canonical_form, verify

def _novelty(seq, refs, novelty=None):
  if novelty == "hard":
    if canonical_form(seq) not in refs:
      return 1.0
    return 0.0
  elif seq not in refs:
    return 1.0
  elif novelty == "soft":
    return 0.3
  else:
    return 0.0

def evaluate(seqs, refs, novelty=None):
  """Check whether the molecule's SMILES is in the reference set, and assign
  value.

  Args:
    seqs (str list): SMILES sequences.
    refs (str list): The reference SMILES sequences.
    novelty (str): Type of novelty to calculate. Possible values are
      ``"hard"``, ``"soft"``, or ``None``. Default is ``None``.

  Returns:
    float list: Scores.
  """
  return [_novelty(seq, refs, novelty=novelty) if verify(seq) else 0
          for seq in seqs]
