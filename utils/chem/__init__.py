"""Various metrics to evaluate chemicals."""

import logging
from .conciseness import evaluate as conciseness
from .diversity import evaluate as diversity
from .druglikeness import evaluate as druglikeness
from .naturalness import evaluate as naturalness
from .novelty import evaluate as novelty
from .solubility import evaluate as solubility
from .synthesizability import evaluate as synthesizability
from .variety import evaluate as variety


def load_metrics(metrics, refs=None):
  """Load predefined metrics.

  Args:
    metrics (str list): List of desired metrics.
    refs (str list, optional): A SMILES set for a reference. Required if
      loading ``diversity`` and ``novelty`` metrics.

  Returns:
    function list: A list of functions calcuates the metric for given SMILES
    sequences.
  """
  log = logging.getLogger(__file__)
  metric_fns = {}
  for metric in metrics:
    if metric == "conciseness":
      metric_fns[metric] = conciseness
    elif metric == "diversity":
      metric_fns[metric] = lambda seqs: diversity(seqs, refs)
    elif metric == "druglikeness":
      metric_fns[metric] = druglikeness
    elif metric == "naturalness":
      metric_fns[metric] = naturalness
    elif metric == "novelty":
      metric_fns[metric] = lambda seqs: novelty(seqs, refs)
    elif metric == "solubility":
      metric_fns[metric] = solubility
    elif metric == "synthesizability":
      metric_fns[metric] = synthesizability
    elif metric == "variety":
      metric_fns[metric] = variety
    else:
      log.warning("Can't find the metric: skip %s", metric)
  return metric_fns
