"""Diversity."""

import numpy as np
from rdkit import DataStructs
from rdkit.Chem import AllChem as Chem
from utils import remap
from utils.smiles import verify_and_apply

LOW_RAND_DST = 0.9
MEAN_DIV_DST = 0.945

def _evaluate(mol, fps):
  ref_fps = Chem.GetMorganFingerprintAsBitVect(mol, 4, nBits=2048)
  dist = DataStructs.BulkTanimotoSimilarity(ref_fps, fps, returnDistance=True)
  mean_dist = np.mean(np.array(dist))
  return remap(mean_dist, LOW_RAND_DST, MEAN_DIV_DST)

def evaluate(seqs, refs):
  """Compares the Tanimoto distance of a given molecule with a random samples
  of the reference set of molecules.

  Args:
    seqs (str list): SMILES sequences.
    refs (str list): The reference SMILES sequences.

  Returns:
    float list: Scores.
  """
  mols = [Chem.MolFromSmiles(seq)
          for seq in np.random.choice(refs, 100)]
  fps = [Chem.GetMorganFingerprintAsBitVect(m, 4, nBits=2048) for m in mols]
  return [verify_and_apply(seq, _evaluate, fps=fps)
          for seq in seqs]
