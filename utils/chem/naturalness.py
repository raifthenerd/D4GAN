"""Naturlaness."""

import os
import math
import gzip
from pickle import load
from rdkit.Chem import AllChem as Chem
from utils import remap
from utils.smiles import verify_and_apply

NP_MODEL = load(gzip.open(os.path.dirname(__file__) + '/NP_score.pkl.gz'))

def _evaluate(mol):
  fp = Chem.GetMorganFingerprint(mol, 2)
  bits = fp.GetNonzeroElements()
  # calculating the score
  score = 0.
  for bit in bits:
    score += NP_MODEL.get(bit, 0)
  score /= float(mol.GetNumAtoms())
  # preventing score explosion for exotic molecules
  if score > 4:
    score = 4. + math.log10(score - 4. + 1.)
  if score < -4:
    score = -4. - math.log10(-4. - score + 1.)
  val = remap(score, -3, 1)
  return val

def evaluate(seqs):
  """This metric computes the likelihood that a given molecule is a natural
  product.

  Args:
    seqs (str list): SMILES sequences.

  Returns:
    float list: Scores.
  """
  return [verify_and_apply(seq, _evaluate)
          for seq in seqs]
