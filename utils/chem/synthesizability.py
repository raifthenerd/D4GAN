"""Synthesizability."""

import os
import math
import gzip
from pickle import load
from rdkit.Chem import AllChem as Chem
from utils import remap
from utils.smiles import verify_and_apply

SA_MODEL = {}
for i in load(gzip.open(os.path.dirname(__file__) + '/SA_score.pkl.gz')):
  for j in range(1, len(i)):
    SA_MODEL[i[j]] = float(i[0])

def _evaluate(mol):
  # fragment score
  fp = Chem.GetMorganFingerprint(mol, 2)
  fps = fp.GetNonzeroElements()
  score1 = 0.
  nf = 0
  for bitid, v in fps.items():
    nf += v
    sfp = bitid
    score1 += SA_MODEL.get(sfp, -4) * v
  score1 /= nf
  # features score
  nAtoms = mol.GetNumAtoms()
  nChiralCenters = len(Chem.FindMolChiralCenters(mol, includeUnassigned=True))
  ri = mol.GetRingInfo()
  nSpiro = Chem.CalcNumSpiroAtoms(mol)
  nBridgeheads = Chem.CalcNumBridgeheadAtoms(mol)
  nMacrocycles = 0
  for x in ri.AtomRings():
    if len(x) > 8:
      nMacrocycles += 1
  sizePenalty = nAtoms**1.005 - nAtoms
  stereoPenalty = math.log10(nChiralCenters + 1)
  spiroPenalty = math.log10(nSpiro + 1)
  bridgePenalty = math.log10(nBridgeheads + 1)
  macrocyclePenalty = 0.
  # ---------------------------------------
  # This differs from the paper, which defines:
  #  macrocyclePenalty = math.log10(nMacrocycles+1)
  # This form generates better results when 2 or more macrocycles are present
  if nMacrocycles > 0:
    macrocyclePenalty = math.log10(2)

  score2 = 0. - sizePenalty - stereoPenalty - \
      spiroPenalty - bridgePenalty - macrocyclePenalty

  # correction for the fingerprint density
  # not in the original publication, added in version 1.1
  # to make highly symmetrical molecules easier to synthetise
  score3 = 0.
  if nAtoms > len(fps):
    score3 = math.log(float(nAtoms) / len(fps)) * .5

  sascore = score1 + score2 + score3

  # need to transform "raw" value into scale between 1 and 10
  sascore = 11. - (sascore + 5.) / 6.5 * 9.
  # smooth the 10-end
  if sascore > 8.:
    sascore = 8. + math.log(sascore + 1. - 9.)
  if sascore > 10.:
    sascore = 10.0
  elif sascore < 1.:
    sascore = 1.0
  val = remap(sascore, 5, 1.5)
  return val

def evaluate(seqs):
  """This metric checks whether a given molecule is easy to synthesize or not.
  It is based on (although not completely equivalent to) the work of Ertl and
  Schuffenhauer. See [#]_ for more information.

  Args:
    seqs (str list): SMILES sequences.

  Returns:
    float list: Scores.

  .. [#] Ertl, P., & Schuffenhauer, A. (2009). Estimation of synthetic
    accessibility score of drug-like molecules based on molecular complexity
    and fragment contributions. Journal of cheminformatics, 1(1), 8.
  """
  return [verify_and_apply(seq, _evaluate)
          for seq in seqs]
