"""Variety."""

import numpy as np
from rdkit import DataStructs
from rdkit.Chem import AllChem as Chem
from utils.smiles import verify_and_apply, drop_invalid

def _evaluate(mol, fps):
  ref_fps = Chem.GetMorganFingerprintAsBitVect(mol, 4, nBits=2048)
  dist = DataStructs.BulkTanimotoSimilarity(ref_fps, fps, returnDistance=True)
  mean_dist = np.mean(np.array(dist))
  return mean_dist

def evaluate(seqs):
  """Compares the Tanimoto distance of a given molecule with a random samples
  of the other generated molecules.

  Args:
    seqs (str list): SMILES sequences.

  Returns:
    float list: Scores.
  """
  filtered = drop_invalid(seqs)
  if len(filtered) < 10:
    return [0] * len(seqs)
  mols = [Chem.MolFromSmiles(seq)
          for seq in np.random.choice(filtered, len(filtered) // 10)]
  fps = [Chem.GetMorganFingerprintAsBitVect(m, 4, nBits=2048) for m in mols]
  return [verify_and_apply(seq, _evaluate, fps=fps)
          for seq in seqs]
