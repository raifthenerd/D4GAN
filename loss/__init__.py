"""Loss functions."""

import math
import random
import torch
import torch.nn.functional as F
from utils import logsumexp
from utils.smiles import decode


def train(model, training, num_samples, lmbda, kappa, tau, metric_fn=None):
  """Loss tensors for training D4GAN.

  Args:
    model (:obj:`model.D4GAN`): D4GAN model.
    training (:obj:`torch.Tensor`): SMILES sequences from training set. The
      dimension is ``[batch_size, seq_len]``.
    num_samples (int): Hyper-parameter to control the number of samples, which
      are used in estimating BGAN style reward.
    lmbda (float): Hyper-parameter to control the ratio of ORGAN style reward;
      should satisfy :math:`0\\leq\\lambda\\leq1`.
    kappa (float): Hyper-parameter to control the behavior for reconstruction;
      should satisfy :math:`0\\leq\\kappa`.
    tau (float): Hyper-parameter to control the teacher forcing ratio;
      should satisfy :math:`0\\leq\\tau\\leq1`.

  Returns:
    (:obj:`torch.Tensor`, :obj:`torch.Tensor`, :obj:`torch.Tensor`):
    Loss tensors; each of them represents the encoder loss, generator loss, and
    discriminator loss respectfully.
  """
  batch_size, _ = training.size()
  mu, log_sigma = model.enc.forward(training)
  sigma = torch.exp(log_sigma)
  loss_kl = 0.5 * torch.mean(torch.sum(mu*mu+sigma*sigma-2*log_sigma-1, 1))

  if random.random() < tau:
    reconstructed, log_g = model.sample(mu + sigma * torch.randn(batch_size, model.feature_dim, device=model.device),
                                      teacher=training)
  else:
    reconstructed, log_g = model.sample(mu + sigma * torch.randn(batch_size, model.feature_dim, device=model.device))
  loss_recon = torch.mean(torch.sum(F.nll_loss(log_g, training, reduce=False), 1))

  fakes, log_g = model.sample(torch.randn(batch_size, model.feature_dim, device=model.device),
                            num_samples)

  log_w = model.dis.forward(fakes)
  loss_gan = torch.mean(F.sigmoid(model.dis.forward(training))) \
             - torch.mean(F.sigmoid(model.dis.forward(reconstructed))) \
             - torch.mean(F.sigmoid(log_w))

  log_w = log_w.view(-1, batch_size) # w: [num_samples, batch_size]
  log_bar_w = logsumexp(log_w - math.log(num_samples), axis=0).detach() # log_bar_w [batch_size]
  log_w_tilde = log_w - math.log(num_samples) - log_bar_w
  w_tilde = torch.exp(log_w_tilde).view(-1).detach() # w_tilde: [num_samples x batch_size]
  if lmbda > 0 and metric_fn:
    o = torch.Tensor(metric_fn([decode(fake) for fake in fakes])).to(model.device)
    rewards = lmbda * o + (1 - lmbda) * w_tilde
  else:
    rewards = w_tilde
  loss_rl = torch.mean(rewards * torch.sum(F.nll_loss(log_g, fakes, reduce=False), 1)) * num_samples
  return (loss_kl + loss_recon, kappa * loss_recon + loss_rl, -loss_gan)
