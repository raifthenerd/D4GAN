Development Environment
-----------------------

- It's highly recommended to use conda_ for managing development environments.

- See `README.rst`_ to check the list of dependencies.

.. _README.rst: README.rst#requirements
.. _conda: https://conda.io/docs

Coding Conventions
-----------------------

- Check `Google Python style guide`_.

  - Use pylint_ to check your code style:

    .. code-block:: bash

       # install pylint
       pip install pylint
       # fetch google style definitions
       wget -O ~/.pylintrc https://raw.githubusercontent.com/tensorflow/tensorflow/master/tensorflow/tools/ci_build/pylintrc
       # check a file
       pylint foo.py

- For PyTorch tensors, denote their dimension in the docstring or on the right
  side. For instance:

  .. code-block:: python

     def foo(x, h):
       """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
       eiusmod tempor incididunt ut labore et dolore magna aliqua.

       Args:
         x (:obj:`torch.Tensor`): The dimension is ``[batch_size]``.
         h (:obj:`torch.Tensor`): The dimension is ``[2, batch_size, hidden_dim]``.

       Returns:
         (:obj:`torch.Tensor`, :obj:`torch.Tensor`): o and h.

         The dimension of o is ``[batch_size, vocab_size]``.

         The dimension of h is ``[2, batch_size, hidden_dim]``.
       """
       o = embeddings(x)                 # o: [batch_size, embedding_dim]
       o = o.view(1, -1, embedding_dim)  # o: [1, batch_size, embedding_dim]
       o, h = two_layer_gru(o, h)        # o: [1, batch_size, hidden_dim]
                                         # h: [2, batch_size, hidden_dim]
       o = fc(o.view(-1, hidden_dim))    # o: [batch_size, vocab_size]
       o = F.log_softmax(o)              # o: [batch_size, vocab_size]
       return o, h

.. _Google Python style guide: https://google.github.io/styleguide/pyguide.html
.. _pylint: https://www.pylint.org

Documentation
-----------------------

- Use Python docstrings. Write docstrings as clear as possible.

  - Check `Google style docstrings`_.

  - Executing ``make html`` under ``docs`` directory makes nicely formatted HTML
    documents, where every text are constructed from the docstrings (You should
    install Sphinx_ first).

.. _Sphinx: http://www.sphinx-doc.org/en/master
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _cheatsheet: https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html
.. _Google style docstrings: https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
